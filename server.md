**RpcServer**
Methos:
*   `check_account`:检测可用账号(车型查询所用账号)

    ```python 
        def check_account(_rs, cookieSet):
            """
            检测账号,保证每一时刻,一个账号只能被一个请求所使用,有的品牌配有多个账号
            """
            global exsistList
            #`exsistList`代表正在用的账号,
            try:
                getList = _rs.smembers(cookieSet)
                if not getList:
                    return None
                lastList = list(getList ^ exsistList)
                #lastList 代表没有被使用的账号 `^`代表差集运算
                setKey = random.choice(lastList)
                exsistList.add(setKey)
                return setKey
            except:
                logging.debug(traceback.format_exc())
                return None
    ```
*   `search`:搜索结果的数据缓存和实时获取,方法会根据传过来的数据中的 vin brand ctype 来确定缓存的 key(搜索结果存储在 redis),通过 key来获取缓存,若没有缓存则实时的去获取 Vin 搜索数据,这里调用了相应车型的 search 方法:

    ```python 
    result, searchDict = model.search(task["vin"])
    #设置缓存的存活时间
    if task["brandCode"] not in ["jaguar"] and r1.ttl(key) < 600:
                        r1.expire(key, random.randint(1200, 1800))
    ```
*   `groups`:直接获取缓存
*   `structInfo`:先获取缓存,没有缓存就实时获取,通过brand 和 num 来确定主组
    ```python
        if task["brandCode"] in "land_rover":
            setStruct = "struct_%s_%s_%s_%s" % (vin, cType, task["brandCode"], num)
        else:       
            setStruct = "struct_%s_%s_%s_%s" % (vin[-7:], cType, task["brandCode"], num)
        
    ```
*   `partInfo`:获取零件数据,通过 auth 来确定图号和主组
    ```python
        if task["brandCode"] in "land_rover":
            setPart = "partnum_%s_%s_%s_%s_%s" % (vin, cType, task["brandCode"], authDict["num"], authDict["mid"])
        else:
            setPart = "partnum_%s_%s_%s_%s_%s" % (vin[-7:], cType, task["brandCode"], authDict["num"], authDict["mid"])
        
    ```