## base 目录

### httpprocess.py

*Class*

* <a id="httpprocess">HttpProcess</a>
  * `__init__`:定义了一些请求时运行参数
  * `_getCookieString`:把 cookie 键值对转换为`=` 号连接`;`作为分割符的字符串
  * `_getCookie`:提供一个 cookie 键,返回对应的值
  * `_initOpener`:创建 opener,关于 [opener ](http://www.iteedu.com/plang/python/urllib2/opener.php)
  * `_getCookieValue`:返回每个 handler 的 cookie 字符串
  * `_retrievePage`: 使用 opener获取页面数据

### base.py

*Class*	

* `Base` :定义了车型继承的基础,涵盖了通用的功能

  * `__init__`:定义了 redis 和request 用到了 httpbase 里面的 [HttpProcess](#httpprocess)

  * `login`:登陆模块    

  * `carList`:获取汽车数据列表

  *  `flatSections`:汽车平面结构

  *   `search`:根据车架号搜索车的信息

  * partNumber`:通过图号获取零件编号

  * `cookieStr`:解析保存的COOKIE

  * `singleFlat`:获取一个汽车结构零件

  * `singlePartNum`:单一零件查询"""   

  * `replacePartNum`:获取部分零件可替换的零件编号映射关系

    ​

以上方法均没有实现,由core 下面的各个 core 文件实现, core 文件再由 car 里面的 coo 实现, coo 来调用 parse 里面的 coo对应的 parse

有一个方法需要注意:

```Python
def urlGrab(self, url, headers, data=None, num=1, **kwargs):
        """"""
        content = None
        if not url:
            logging.info("url [%s] is None." % url)
            return None
        sTime = time.time()
        #logging.info("startTime [%s]" % sTime)
        i = 0
        proxy = {}
        while i < num:
            if kwargs.get("proxy",False):
                proxy = self.getProxy()
                logging.debug("proxy %s" % str(proxy))
            if data or kwargs.has_key("post"):
                req = urllib2.Request(url, headers=headers, data=data)
            else:
                req = urllib2.Request(url, headers=headers)
            #req.set_proxy(proxy["http"], "https")
            if kwargs.has_key("timeout"):
                content = self.r._retrievePage(req, timeout=kwargs["timeout"], proxy=proxy)            
            else:
                content = self.r._retrievePage(req, timeout=10, proxy=proxy)
            if content:
                break
            i+=1
            time.sleep(i)
            logging.info("[%s], %dnd fail, useTime[%s], code[%d], proxy[%s]." % (url, i, str(time.time()-sTime), self.r.code, str(proxy)))
        logging.info("[%s], useTime[%s], code[%d], proxy[%s]." % (url, str(time.time()-sTime), self.r.code, str(proxy)))
        return content
```

​	urlGrab 定义了页面的获取流程和重试机制,运用之处很多,本质是调用 httpbase 里面的 urllib2的 opener 来实现页面获取.

## 其他

其他的文件类似 beanstalkd.py 是操作 beanstalk 的 mydb 是操作 mysql 数据库的,代码简单不再赘述

