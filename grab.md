##	目录



* [baseGrap.py](#baseGrap)
* [authCookie.py](#authCookie)
* [syncstore.py](#syncstore)

## <a id="baseGrap">baseGrap.py</a>

**Class**

* `Worker`

  woker简单继承了 `threading`的 Thread

**Method**

* `unicode2utf8` :转换输入对象为utf8格式

* `findModelname`:返回 `car`目录所有的品牌解析文件名(去除`. py` 后缀名),结果`c001 c002 c003 c004 `等

* `checkBc`:检查 beanstalk 是否可用

* `worker_cars`:根据配置的任务列表等获得车型列表,不断从 `beanstalk` 尝试获取`task`,若获取的 job 的 `ctype` 不对就删除掉错误的 job,若获得 `task`,从 [task](#task) 获取其存取的信息,通过task 获取获得 `ctyp` ,通过` ctype`获得 redis中存储的 cookie(之前登录设置的),如果没有相应的 cookie 说明需要尝试用账号密码登录,并把得到的 cookie 再次存放在 redis 中.你可能没有在本文件看到 cookie 的存储,因为存储cookie 的操作在 `pool/cookie.py`中操作,这里只负责读取 cookie. 

  ```python
   	carList, flatList = model.carList()
      carDict = {"sql":"carInfo"}
      for cars in carList:
          setDict = deepcopy(carDict)
          setDict["data"] = cars
          putJob(bc, setDict, target_tube="store")
  ```

  上面这段代码,获取基本车型的信息,然后把[存储任务](#task1)放到管道中,对应表` cars_info_collection`

* `worker_groups`:和上面的逻辑一样,不过是存储主组数据,对应表`cars_group_collection`

* `worker_struction`:抓取存储分组数据,对应表` cars_structure_collection`

* `worker_partnum`:抓取零件数据,对应表`cars_part_num`

* `worker_img`:抓取图片,更新分组表

* `start` :根据命令行参数来运行对应的任务

* `delCache`:操作次数归零

<a id="task">Task</a>

 <a id="task1">*task1*</a>定义了任务的需要的信息:

```javascript
{
  "data":flatDict,
  "sql":"groupInfo"
}
```

<a id="task2">*ask2*</a> 上面的 job 存储了登录需要的信息,用以实现登录

```javascript
{
  	"username":"admin",
  	"pwd":"password",
  	"brandCode":"bwm",
    "ctype":006,
}
```



 

> ​	代码有重复,需要合并

## <a id="authCookie">authCookie.py</a>

**Class **

* *AuthCookie*(deprecated use `pool/cookie`)
  * *method*
    * `findModelname`:返回所有车型对应 `car`文件夹下所有` c**.py`,需要提取为 util 
    * `fetchModelClass`:根据 taskid,task 返回一个初始化的车型对象
    * `cookies`:验证 cookie,清理无效的 cookie, 并设置新的 cookie
    * `start`
    * `task`

## <a id="syncstore">syncstore.py</a>

**Class**

* `StoreCar`

  * `__init__`:封装了一个 <a id="base">Base</a> 对象,主要用到了该对象的 data2db 方法

    ```python
    def data2db(self, _db, func, data):
            """数据写入到数据库"""
            dataList = []
            errorData = ""
            result = True
            if isinstance(data, dict):
                dataList = [data]
            elif isinstance(data, list):
                dataList = data
            else:
                logging.error("type error")
                return False , ""
            for getData in dataList:
                try:
                    getData = self.unicode2utf8(getData)
                    sql = func(getData)
                    logging.info(repr(sql))
                    _db.ExecSQL(sql)
                    #_db.commit()  
                except Exception, e:
                    logging.error(format_exc())
                    result = False
                    errorData = e.__str__()
                    break
    ```

    ​

  * `checkBc`:测试 beanstalk 连接

  * `checkMysql`:测试数据库连接

  * `start` 从 beanstalk 获取数据来存数据

    ```python
    while True:
                #time.sleep(1)
                try:
                    #从beanstalk管道获取一个任务
                    job, task = getJob(bc, tp="json")
                    if not job:
                        time.sleep(1)
                        logging.debug("threadNum[%s],wait a store job" % threadNum)
                        continue
                    getSql = task["sql"]
                    data = task["data"]
                    if getSql == "vinInfo":
                        sqlFunc = self.base.setVinInfoSql
                    elif getSql == "carInfo":
                        sqlFunc = self.base.setCarInfoSql
                    elif  getSql == "groupInfo":
                        sqlFunc = self.base.setGroupInfoSql
                    elif getSql == "structionInfo":
                        sqlFunc = self.base.setStructionInfoSql
                    elif getSql == "partInfo":
                        sqlFunc = self.base.setPartInfoSql
                    elif getSql == "imgsinfo":
                        sqlFunc = self.base.setImgInfoSql
                    else:
                        time.sleep(1)
                        logging.debug("threadNum[%s],getSql[%s]" % (threadNum, getSql))
                        continue
                    result, logData = self.base.data2db(_db, sqlFunc, data)
    ```

    ​

  

    ​

